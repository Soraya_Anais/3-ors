// import { Link } from "@chakra-ui/react";
// import React from "react";
// import { NavLink } from "react-router-dom";

// export const Navbar = () => {
//   return (
//     <>
//       <Link to="/" as={NavLink}>
//         Home
//       </Link>
//       <Link to="/contactUs" as={NavLink}>
//         Contact
//       </Link>
//       <Link to="/account" as={NavLink}>
//         Account
//       </Link>
//       <Link to="/bag" as={NavLink}>
//         Bag
//       </Link>
import { Flex, Heading, Link, Text, useDisclosure } from "@chakra-ui/react";
import React from "react";
import { NavLink, useNavigate, useLocation } from "react-router-dom";
import styles from "../style/navbar.module.css";
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from "@chakra-ui/react";
import { MydrawerContent } from "./MydrawerContent";

export const Navbar = ({setQuery}) => {
  const { isOpen, onClose } = useDisclosure();
   const navigate = useNavigate();
   const location = useLocation();
   const handleNavigateToRings = () => {
    navigate("/rings");
  };
  const handleNavigateToBraces = () => {
    navigate("/braces");
  };
  const handleNavigateToCollars = () => {
    navigate("/collars");
  };
  const handleNavigateToEarrings = () => {
    navigate("/earrings");
  };
  return (
    <>
      <Flex
        justify="space-around"
        flexDirection={{base : "column" , md:"row" , lg : "row" , xl : "row"}}
        alignItems="center"
        zIndex={"2"}
        p="4"
        pl="60px"
        pr="60px"
        h={{base : "35vh" , md:"10vh" , lg : "10vh" , xl : "10vh"}}
        w="100%"
        className={styles.navbaronly}
      >

        <Text
          onClick={handleNavigateToCollars}
          p="1"
          _hover={{ cursor: "pointer" }}
          color={location.pathname === "/collars" ? "gray" : "white"}
        >
          Colliers
        </Text>
        <Text
          onClick={handleNavigateToBraces}
          p="1"
          _hover={{ cursor: "pointer" }}
          color={location.pathname === "/braces" ? "gray" : "white"}
        >
          Bracelets
        </Text>

        <Link to="/" as={NavLink}>
          <Heading as="h4" fontSize="25px" color="white">
            Les 3-Ors
          </Heading>
        </Link>

        <Text
          onClick={handleNavigateToRings}
          p="1"
          _hover={{ cursor: "pointer" }}
          color={location.pathname === "/rings" ? "gray" : "white"}
        >
          Bagues
        </Text>
        
        <Text
          onClick={handleNavigateToEarrings}
          p="1"
          _hover={{ cursor: "pointer" }}
          color={location.pathname === "/earrings" ? "gray" : "white"}
        >
          Boucles
        </Text>

        {/* <Link to="/contactUs" as={NavLink} p="1" color={location.pathname === "/contactUs" ? "gray" : "white"}
        >
          Contact
        </Link> */}
        {/* <Link to="/account" as={NavLink} p="1">
          Account
        </Link>
        <Link to="/bag" as={NavLink} p="1">
          Bag
        </Link> */}
      </Flex>
      {/* {isAuth ? <Button pos={"fixed"} color={"white"} zIndex={5} right={"12px"} top={"10px"} colorScheme='white' variant='ghost' onClick={()=>dispatch({type:LOGOUT})}>
    Logout
  </Button> 
  : <Button pos={"fixed"} right={"12px"}  color={"white"} zIndex={5} top={"10px"}  colorScheme='white' variant='ghost' onClick={()=>{navigate("/account")}}>
    Login
  </Button>
  } */}
      <Drawer onClose={onClose} isOpen={isOpen} size="md" placement="left">
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>{`Gem Garden`}</DrawerHeader>
          <DrawerBody>
            <MydrawerContent setQuery={setQuery}/>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
};
