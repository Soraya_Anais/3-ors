import React from "react";
import styles from "../style/Abouts.module.css";
import fbicon from "../Pics/icons8-facebook-color-48.png"
import twittericon from "../Pics/icons8-twitter-color-48.png"
import instaicon from "../Pics/icons8-instagram-color-48.png"
import whatsappicon from "../Pics/icons8-whatsapp-color-48.png"
import { Helmet } from 'react-helmet';

const Abouts = () => {
  return (
    <section
      className={styles.HomePage_sectionP1__kiHi}
      id={styles.HomePage_about__KBFWW}
    >
      <div className={styles.HomePage_firstDiv__2J3gv}>
      <Helmet>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Bona+Nova+SC:ital,wght@0,400;0,700;1,400&display=swap"
          rel="stylesheet"
        />
      </Helmet>
        <div className={styles.HomePage_frstDiv1}>
          <h1 style={{ fontFamily: 'Bona Nova SC, sans-serif' }}>A PROPOS DE NOUS</h1>
          <p>Les 3-Ors is More Than Just Gliding</p>
          <p>
            Our Company is an exclusive supplier of jewelry from the world's
            best brands. We take pride in offering our customers only the
            highest quality products created from precious metals and stones by
            the most experienced master jewelers
          </p>
          <p>
            We are constantly expanding our range to meet the needs of our
            customers and offer them the latest and most fashionable trends in
            jewelry. We are confident that our collection of jewelry will allow
            everyone to express their individual style and create a unique image
          </p>
          {/* <span>OUR STORES</span> */}
        </div>
        <div className={styles.HomePage_frstDiv2}>
          <h1>3-ORS</h1>
        </div>
      </div>
      <div
        className={styles.HomePage_secondDiv__lALhm}
      >
        <div className={styles.divimge}>
          <img
            src="https://gem-garden-jewelry-store.vercel.app/static/media/meeting.c96f8779b2d55da20832.png"
            alt="Meeting img"
          />
        </div>
        <div>
          <h1>Entrez en contact avec nous</h1>
          <p style={{fontSize:"20px"}}>Rejoignez nous sur nos réseaux</p>
        <div style={{  display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-around", marginLeft:"-50px"}}>
          <div className={styles.foot_social_media_Div__djhh7}>
          <a href="https://www.facebook.com">
          <img src={fbicon} style={{width:"50px"}}  alt="facebook-logo"/>
          </a>
          {/* <span style={{ color: "black" }}>Facebook</span> */}
          </div>
          <div className={styles.foot_social_media_Div__djhh7}>
            <a href="https://www.twitter.com/">
            <img src={twittericon}  style={{width:"50px"}}  alt="twitter-logo"/>
            </a>
            {/* <span style={{ color: "black" }}>Twitter</span> */}
          </div>
          <div className={styles.foot_social_media_Div__djhh7}>
            <a href="https://www.instagram.com/">
            <img src={instaicon}  style={{width:"50px"}}  alt="insta-logo"/>
            </a>
            {/* <span style={{ color: "black" }}>Instagram</span> */}
          </div>
          <div className={styles.foot_social_media_Div__djhh7}>
            <a href="https://www.whatsapp.com/">
            <img src={whatsappicon}  style={{width:"50px"}}  alt="whatsapp-logo"/>
            </a>
            {/* <span style={{ color: "black" }}>Whatsapp</span> */}
          </div>
        </div>
          {/* <span className={styles.spantag}>LEARN MORE</span> */}
        </div>
      </div>
    </section>
  );
};

export default Abouts;
