import React from 'react'
import styles from '../style/mainPage.module.css'
import { Box, Flex, HStack, Heading, Spacer, Text, VStack } from '@chakra-ui/react'
export const MainPagemainComponent = () => {
  return (
    <Flex className={styles.stopmostcontainer} alignItems={"end"} w={{base : "140%", md:"100%"}} pt={{base:"200px", md:"40px"}} flexDirection={{base : "column",md:"row"}}>
    <HStack w='30%' h='90%' marginLeft='10%'>
      <VStack alignItems='start'>
        <Heading as='h2' textAlign='left' m='1' color='white'>Bienvenu chez les 3-Ors</Heading>
        <Text textAlign='left' fontSize='lg'  color='white'>Inspirez vous de nos oeuvres ...</Text>
        <br />
        <br />
        {/* <Text textAlign='left'  color='white'>DISCOVER</Text> */}
      </VStack>
     
    </HStack>
 <Spacer />
    <Box w='60%' className={styles.mainhands}>
      </Box>
  </Flex>
  )
}
