import React from "react";
import styles from "../style/Footer.module.css";
import fbicon from "../Pics/icons8-facebook-48.png"
import twittericon from "../Pics/icons8-twitter-50.png"
import instaicon from "../Pics/icons8-instagram-48.png"
import whatsappicon from "../Pics/icons8-whatsapp-50.png"
import {Text } from '@chakra-ui/react'
import {useNavigate} from "react-router-dom";

const Footer = () => {
  const navigate = useNavigate();
  const handleNavigateToRings = () => {
    navigate("/rings");
  };
  const handleNavigateToBraces = () => {
    navigate("/braces");
  };
  const handleNavigateToCollars = () => {
    navigate("/collars");
  };
  const handleNavigateToEarrings = () => {
    navigate("/earrings");
  };
  return (
    <div className={styles.foot_footerComponent__BtWvP}>
      <div className={styles.foot_app_Title__zA_uX}>
        <h2>LES 3-ORS</h2>
      </div>
      <div className={styles.foot_contacts__yLyoe}>
        <p><b style={{ color: "white" }}>Contact Us</b></p>
        <br/>
        <div className={styles.foot_social_media_Div__djhh7}>
          <a href="https://www.facebook.com">
          <img src={fbicon} style={{width:"30px"}} alt="facebook-logo"/>
          </a>
          <span style={{ color: "white" }}>Facebook</span>
        </div>
        <div className={styles.foot_social_media_Div__djhh7}>
          <a href="https://www.twitter.com/">
          <img src={twittericon}  style={{width:"30px"}} alt="twitter-logo"/>
          </a>
          <span style={{ color: "white" }}>Twitter</span>
        </div>
        <div className={styles.foot_social_media_Div__djhh7}>
          <a href="https://www.instagram.com/">
          <img src={instaicon}  style={{width:"30px"}}  alt="insta-logo"/>
           </a>
           <span style={{ color: "white" }}>Instagram</span>
        </div>
        <div className={styles.foot_social_media_Div__djhh7}>
          <a href="https://www.instagram.com/">
          <img src={whatsappicon}  style={{width:"30px"}}  alt="whatsapp-logo"/>
            </a>
            <span style={{ color: "white" }}>WhatsApp</span>
        </div>
      </div>
      <div className={styles.foot_jewelry_Type__p6DGh}>
        <Text fontSize={"16px"} _hover={{
            textDecoration: "underline"
        }} onClick={handleNavigateToRings} >Bagues</Text>
        <Text fontSize={"16px"} _hover={{
            textDecoration: "underline"
        }} onClick={handleNavigateToBraces}>Bracelets</Text>
        <Text fontSize={"16px"} _hover={{
            textDecoration: "underline"
        }} onClick={handleNavigateToEarrings}>Boucles</Text>
        <Text fontSize={"16px"} _hover={{
            textDecoration: "underline"
        }} onClick={handleNavigateToCollars}>Colliers</Text>
      </div>
      <div className={styles.foot_email_Section__qc_3a}>
        <div className={styles.foot_policy__FkJCy}>
          <p>Legal notice</p>
          <p>Privacy policy</p>
        </div>
        <div className={styles.foot_copyright}>
          <p style={{color:"rgb(62, 61, 61)"}}>© 2024 les 3-ors ! Tous droits reservés</p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
