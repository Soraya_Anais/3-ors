// src/Rings.js
import {
    Box,
    Heading,
    Image,
    Text,
    SimpleGrid,
    Flex,
  } from "@chakra-ui/react";
  import React from "react";
  
  const BracesProducts = [
    {
      id: 1,
      name: "Messika",
      description: "White Gold Diamond Ring.",
      price: "$1,390",
      imageUrl: "https://gem-garden-jewelry-store.vercel.app/static/media/card5.471f0f73c03a3ce42177.png",
    },
    {
      id: 2,
      name: "Tiffani",
      description: "Silver Necklace.",
      price: "$750",
      imageUrl: "https://gem-garden-jewelry-store.vercel.app/static/media/card6.cdfc389c353e0a1a95c5.png",
    },
    {
      id: 3,
      name: "Bulgari",
      description: "White Gold Diamond Earring.",
      price: "$7,250",
      imageUrl: "https://gem-garden-jewelry-store.vercel.app/static/media/card7.3abfa35a89cf47b097ae.png",
    },
    {
      id: 4,
      name: "Cartier",
      description: "White Gold Ring.",
      price: "$1,950",
      imageUrl: "https://mygemma.com/cdn/shop/products/122588-fv_1200x.jpg?v=1659050494",
    },
    {
      id: 5,
      name: "Cartier",
      description: "White Gold Ring.",
      price: "$1,950",
      imageUrl: "https://mygemma.com/cdn/shop/products/122588-fv_1200x.jpg?v=1659050494",
    },
  ];
  
  const Braces = () => {
    return (
      <Box m="30px">
        <Box w="90%" m="auto">
          <Flex justifyContent={"space-between"} alignItems={"center"}>
            <Box textAlign={"left"} color={"#171616"} fontWeight={"400"}>
              <Heading
                size={"lg"}
                fontWeight={"400"}
                textTransform={"uppercase"}
                color={"black"}
              >
                BRACELETS
              </Heading>
              <Text fontSize={"16px"}>
                Décrouvrez nos articles à prix très abordables
              </Text>
            </Box>
          </Flex>
        </Box>
  
        <SimpleGrid
          spacing={6}
          templateColumns={{
            base: "repeat(1, 1fr)",
            md: "repeat(2, 1fr)",
            lg: "repeat(3, 1fr)",
            xl: "repeat(4, 1fr)",
          }}
          justifyContent="space-around"
          width="95%"
          pl="5%"
          pr="5%"
          pb="5%"
          m={"auto"}
        >
          {BracesProducts.map((brace) => (
            <Box
              key={brace.id}
              boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px"
              _hover={{ transform: "scale(1.05)", transition: "transform 0.4s" }}
              h={"300px"}
            >
              <Image src={brace.imageUrl} w="200px" m={"auto"} />
              <Heading m="10px" as="h6" size={"sm"} textAlign="left">
                {brace.name}
              </Heading>
              <Text m="10px" fontSize={"14px"}>
                {brace.description}
              </Text>
              <Text m="10px" fontSize={"14px"} color="gray">
                {brace.price}
              </Text>
            </Box>
          ))}
        </SimpleGrid>
      </Box>
    );
  };
  
  export default Braces;
  