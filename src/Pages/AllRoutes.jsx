import React from "react";
import { Route, Routes } from "react-router-dom";
import { Home } from "./Home";
import { ContactUs } from "./ContactUs";
import Rings from './Rings';
import Collars from './Collars';
import Braces from './Braces';
import Earrings from './Earrings';
// import { Bag } from "./Bag";
// import { Account } from "./Account";
// import { PrivateRoute } from "./PrivateRoute";
import Product from "./Product";

export const AllRoutes = ({query}) => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/rings" element={<Rings />} />
      <Route path="/collars" element={<Collars />} />
      <Route path="/braces" element={<Braces />} />
      <Route path="/earrings" element={<Earrings />} />
      <Route path="/contactUs" element={<ContactUs />} />
      <Route path="/products" element={<Product query={query}/>} />

    </Routes>
  );
};
